import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register (){

	const navigate = useNavigate();
	const { user } = useContext(UserContext)
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [isActive, setIsActive] = useState(false);

	//console.log(email);
	//console.log(password1);
	//console.log(password2);

	function registerUser(e) {

		e.preventDefault()

		fetch('http://localhost:4000/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {
				Swal.fire({
                        title: "Duplicate Email Found",
                        icon: "error",
                        text: "Please provide a different email"
                     }) 
			} else {
				fetch('http://localhost:4000/users/register', {
							method: 'POST',
							headers: {
								'Content-Type': 'application/json'
							},
							body: JSON.stringify({
								firstName: firstName,
								lastName: lastName,
								email: email,
								password: password1,
								mobileNo: mobileNumber
							})
						})
						.then(res => res.json())
						.then(data => {

							if(data) {
								Swal.fire({
								    title: "Success",
								    icon: "success",
								    text: "You have successfully registered"
								})

								setFirstName('');
								setLastName('');
								setEmail('');
								setPassword1('');
								setPassword2('');
								setMobileNumber('');

								navigate('/login')
							} else {
								Swal.fire({
				                        title: "Failed",
				                        icon: "error",
				                        text: "Check your details and try again!"
				                     }) 
							}
						})
						

			}
		})

		//alert('Thank you for registering!');
	}

	useEffect(() => {

		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNumber !== '') && (mobileNumber.length === 11) && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false)
		}

	}, [firstName, lastName, email, password1, password2, mobileNumber])



	return(
		
		(user.id !== null) ? 

			<Navigate to="/courses"/>
			
			:

			<Form className="mt-3" onSubmit = {(e) => registerUser(e)} class>
			  <Form.Group className="mb-3" controlId="firstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control 
			    				type="text" 
			    				placeholder="Enter First Name"
			    				value = {firstName}
			    				onChange = {e => setFirstName(e.target.value)}
			    				required />
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="lastName">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control 
			    				type="text" 
			    				placeholder="Enter Last Name"
			    				value = {lastName}
			    				onChange = {e => setLastName(e.target.value)}
			    				required />
			  </Form.Group>
			  <Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    				type="email" 
			    				placeholder="Enter email"
			    				value = {email}
			    				onChange = {e => setEmail(e.target.value)}
			    				required />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    				type="password" 
			    				placeholder="Password"
			    				value={password1}
			    				onChange= {e => setPassword1(e.target.value)}
			    				required />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    				type="password" 
			    				placeholder="Verify Password"
			    				value={password2}
			    				onChange= {e => setPassword2(e.target.value)}
			    				required />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="mobileNumber">
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control 
			    				type="text" 
			    				placeholder="Enter your mobile number"
			    				value={mobileNumber}
			    				onChange={e=> setMobileNumber(e.target.value)}
			    				required />
			  </Form.Group>

			  {
			  	isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  		:
			  		<Button variant="danger" type="submit" id="submitBtn" disabled>
			  		  Submit
			  		</Button>
			  }

			</Form>
		
		)

}

/*

	Mini-Activity:
		If the user goes to /register route (Registration Page) when the user is already logged in, navigate/redirect to the /courses route (Courses Page)

		6:30 PM

*/